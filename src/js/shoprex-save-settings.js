// Ajax function
function shoprex_bootstrap_prepare_ajax(field, value, animation, message_anchor) {
    var opts = {
        url: ajaxurl, // ajaxurl is defined by WordPress and points to /wp-admin/admin-ajax.php
        type: 'POST',
        async: true,
        cache: false,
        dataType: 'json',
        data: {
            action: 'bootstrap_save_shoprex_settings', // Tell WordPress how to handle this ajax request
            field: field,
            value: value,
            security: shoprex_js_translation.security
        },
        success: function (response) { // success refers to the success of the ajax function itself, not the callback result
            animation.hide(); // Hide the loading animation

            if (true === response.success) {
                //message_anchor.after( '<div id="message" style="display: none;" class="updated below-h2"><p>' + shoprex_js_translation.option_saved + '</p></div>' );
                //jQuery('#message').fadeIn();
                return;
            } else {
                message_anchor.after('<div id="message" style="display: none;" class="error below-h2"><p>' + shoprex_js_translation.option_not_saved_1 + '</p></div>');
                jQuery('#message').fadeIn();
                return;
            }

        },
        error: function (xhr, textStatus, e) {
            message_anchor.after('<div id="message" style="display: none;" class="error below-h2"><p>' + shoprex_js_translation.option_not_saved_2 + '</p></div>');
            jQuery('#message').fadeIn();
            return;
        }
    };
    return opts;
}

// Single radio element
jQuery(document).ready(function ($) {

    $('input.settings-select').change(function (obj) {

        var field = $(this).attr('name');
        var value = $(this).val();
        var animation = $('#loading-animation');
        animation.show();
        var page_title = $('#page-title');
        var message = $('#message');
        message.remove();

        var opts = shoprex_bootstrap_prepare_ajax(field, value, animation, page_title);

        $.ajax(opts);
    });
});

// All input elements
function shoprex_bootstrap_save_cdn() {
    var field = jQuery("input[class=settings-input]").map(function () {
        return jQuery(this).attr('name');
    }).get().toString();
    var value = jQuery("input[class=settings-input]").map(function () {
        return jQuery(this).val();
    }).get().toString();

    var animation = jQuery('#loading-animation');
    animation.show();
    var page_title = jQuery('#page-title');
    var message = jQuery('#message');
    message.remove();

    var opts = shoprex_bootstrap_prepare_ajax(field, value, animation, page_title);

    jQuery.ajax(opts);

}