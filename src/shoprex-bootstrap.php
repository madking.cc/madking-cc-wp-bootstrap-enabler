<?php
/*
Plugin Name: Shoprex Bootstrap Enabler
Plugin URI: https://shoprex.de/en/wordpress_plugins.php
Description: Enable bootstrap for the frontend.
Version: 1.0
Author: Andreas Rex
Author URI: https://shoprex.de/en/profile.php
License: GNU GPL v2.0
Text Domain: shoprex-bootstrap
Domain Path: /lang/
*/

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Slug
$shoprex_slug                = "shoprex";
$shoprex_bootstrap_slug_menu = "shoprex_bootstrap_settings";

/// Helper Functions ///

// Set DB Keyname
function shoprex_bootstrap_format_db_keyname( $string ) {
	$string = strtolower( $string );
	$string = str_replace( "-", "_", $string );

	return $string;
}

// Function to set Database values
function shoprex_bootstrap_set_db_value( $db_string, $db_value ) {
	wp_cache_delete( 'alloptions', 'options' ); // Delete Cache for correct result

	if ( ( $result = get_option( shoprex_bootstrap_format_db_keyname( $db_string ) ) ) !== false ) { // check if db record already exist
		if ( $result != $db_value )   // If yes, check if value has changed
		{
			$result = update_option( shoprex_bootstrap_format_db_keyname( $db_string ), $db_value );
		} // If yes, set new value
		else {
			return true;
		} // If no, send ok
	} else {
		$result = add_option( shoprex_bootstrap_format_db_keyname( $db_string ), $db_value );  // Create new record
	}

	return $result;  // return result
}

// Function to delete Database values
function shoprex_bootstrap_delete_db_entry( $db_string ) {
	wp_cache_delete( 'alloptions', 'options' ); // Delete Cache for correct result

	if ( ( $result = get_option( shoprex_bootstrap_format_db_keyname( $db_string ) ) ) !== false ) { // check if db record already exist
		delete_option( shoprex_bootstrap_format_db_keyname( $db_string ) );
	}

	wp_cache_delete( 'alloptions', 'options' ); // Delete Cache for correct result
}

//// Hooks ////

// Load Translation
function shoprex_bootstrap_load_textdomain() {
	load_plugin_textdomain( 'shoprex-bootstrap', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
}

add_action( 'plugins_loaded', 'shoprex_bootstrap_load_textdomain' );

// Hook to load scripts and stylesheets
function shoprex_bootstrap_load_script_style( $hook ) {

	// Load only on ?page=mypluginname
	if ( $hook == 'settings_page_shoprex_bootstrap_settings' ) {

		// Include Javascript
		wp_enqueue_script( 'shoprex-bootstrap-save-settings', plugins_url( '/js/shoprex-save-settings.js', __FILE__ ), array( 'jquery' ), '', true );
		wp_localize_script( 'shoprex-bootstrap-save-settings', 'shoprex_js_translation', array(
			'security'           => wp_create_nonce( 'shoprex-bootstrap-nonce' ),
			'option_saved'       => esc_html__( 'Setting saved', 'shoprex-bootstrap' ),
			'option_not_saved_1' => esc_html__( 'Setting not saved, error code 1', 'shoprex-bootstrap' ),
			'option_not_saved_2' => esc_html__( 'Setting not saved, error code 2', 'shoprex-bootstrap' ),
		) );
		// Include CSS
		wp_enqueue_style( 'shoprex-bootstrap-settings', plugins_url( '/css/shoprex-bootstrap.css', __FILE__ ) );
	}
}

add_action( 'admin_enqueue_scripts', 'shoprex_bootstrap_load_script_style' );


// Callback function for hook menu
function shoprex_bootstrap_add_admin_submenu( $hook ) {
	global $shoprex_bootstrap_slug_menu;

	$args = "manage_options";

	add_submenu_page( "options-general.php", esc_html__( 'Shoprex Bootstrap Settings', 'shoprex-bootstrap' ), esc_html__( 'Shoprex Bootstrap', 'shoprex-bootstrap' ), $args, $shoprex_bootstrap_slug_menu, "shoprex_bootstrap_callback_admin_submenu" );
}

add_action( "admin_menu", "shoprex_bootstrap_add_admin_submenu" );

// Callback function to display settings page
function shoprex_bootstrap_callback_admin_submenu() {

	$content = "";

	if ( ! current_user_can( 'manage_options' ) ) {
		$content .= "<p>" . esc_html__( 'You do not have permission to access this page.', 'shoprex-jquery' ) . "</p>";
	} else {

		$shoprex_bootstrap_css_enable   = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_css_enable" ) );
		$shoprex_bootstrap_js_enable    = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_js_enable" ) );
		$shoprex_bootstrap_theme_enable = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_theme_enable" ) );

		$shoprex_bootstrap_css_url   = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_css_url" ) );
		$shoprex_bootstrap_js_url    = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_js_url" ) );
		$shoprex_bootstrap_theme_url = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_theme_url" ) );

		$content .= "<div class='wrap'>\n";
		$content .= "<div class='icon32'><br /></div>\n";
		$content .= "<h2 id='page-title'>" . esc_html__( 'Shoprex Bootstrap Settings', 'shoprex-bootstrap' ) . " <img src='" . esc_url( admin_url() . '/images/loading.gif' ) . "' id='loading-animation' style='display:none' /></h2>\n";
		$content .= "<h3>" . esc_html__( 'Main section', 'shoprex-bootstrap' ) . "</h3>\n";
		$content .= "<p><span class='col1'>" . esc_html__( 'Complete CSS', 'shoprex-bootstrap' ) . "</span><span>
<input class='settings-select' type='radio' value='1' name='shoprex_bootstrap_css_enable'";
		if ( $shoprex_bootstrap_css_enable ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'on', 'shoprex-bootstrap' ) . "\n
<input class='settings-select' type='radio' value='0' name='shoprex_bootstrap_css_enable'";
		if ( ! $shoprex_bootstrap_css_enable ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'off', 'shoprex-bootstrap' ) . "\n
</span></p>\n";

		$content .= "<p><span class='col1'>" . esc_html__( 'Complete JavaScript', 'shoprex-bootstrap' ) . "</span><span>
<input class='settings-select' type='radio' value='1' name='shoprex_bootstrap_js_enable'";
		if ( $shoprex_bootstrap_js_enable ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'on', 'shoprex-bootstrap' ) . "\n
<input class='settings-select' type='radio' value='0' name='shoprex_bootstrap_js_enable'";
		if ( ! $shoprex_bootstrap_js_enable ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'off', 'shoprex-bootstrap' ) . "\n
</span></p>\n";

		$content .= "<p><span class='col1'>" . esc_html__( 'Optional Theme', 'shoprex-bootstrap' ) . "</span><span>
<input class='settings-select' type='radio' value='1' name='shoprex_bootstrap_theme_enable'";
		if ( $shoprex_bootstrap_theme_enable ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'on', 'shoprex-bootstrap' ) . "\n
<input class='settings-select' type='radio' value='0' name='shoprex_bootstrap_theme_enable'";
		if ( ! $shoprex_bootstrap_theme_enable ) {
			$content .= " checked";
		}
		$content .= ">" . esc_html__( 'off', 'shoprex-bootstrap' ) . "\n
</span></p>\n";

		$content .= "<h3>" . esc_html__( 'Settings section', 'shoprex-bootstrap' ) . "</h3>\n";
		$content .= "<p><span class='col1'>" . esc_html__( 'CDN Url for CSS:', 'shoprex-bootstrap' ) . "</span> <input class='settings-input' type='text' name='shoprex_bootstrap_css_url' value='" . $shoprex_bootstrap_css_url . "' size='100'></p>\n";
		$content .= "<p><span class='col1'>" . esc_html__( 'CDN Url for JavaScript:', 'shoprex-bootstrap' ) . "</span> <input class='settings-input' type='text' name='shoprex_bootstrap_js_url' value='" . $shoprex_bootstrap_js_url . "' size='100'></p>\n";
		$content .= "<p><span class='col1'>" . esc_html__( 'CDN Url for Theme:', 'shoprex-bootstrap' ) . "</span> <input class='settings-input' type='text' name='shoprex_bootstrap_theme_url' value='" . $shoprex_bootstrap_theme_url . "' size='100'></p>\n";
		$content .= "<p><span class='col1'></span> <button onclick='shoprex_bootstrap_save_cdn()'>" . esc_html__( 'Save', 'shoprex-bootstrap' ) . "</button></p>\n";
		$content .= "</div>\n";
	}
	echo $content;
}

// Ajax save settings page
function shoprex_bootstrap_save_settings() {
	// Verify Nonce
	check_ajax_referer( "shoprex-bootstrap-nonce", "security" );
	// Check user role
	if ( ! current_user_can( 'manage_options' ) ) {
		wp_die( esc_html__( 'You do not have permission to access this page.', 'shoprex-bootstrap' ) );
	}

	$field = explode( ",", $_POST['field'] );
	$value = explode( ",", $_POST['value'] );

	foreach ( $field as $key => $string ) {
		$result = shoprex_bootstrap_set_db_value( $string, $value[ $key ] );
		if ( $result == false ) {
			break;
		}
	}

	if ( $result ) {
		wp_send_json_success();
	} else {
		wp_send_json_error();
	}

}

add_action( 'wp_ajax_bootstrap_save_shoprex_settings', 'shoprex_bootstrap_save_settings' );

// Register Hook
function shoprex_bootstrap_activate_plugin() {
	register_uninstall_hook( __FILE__, 'shoprex_bootstrap_uninstall_plugin' );

	// Include data file
	require( plugin_dir_path( __FILE__ ) . "shoprex-data.php" );

	foreach ( $jquery_bootstrap_default_settings as $array ) {
		shoprex_bootstrap_set_db_value( $array['db_name'], $array['cdn_uri'] );
	}
}

register_activation_hook( __FILE__, 'shoprex_bootstrap_activate_plugin' );

// Uninstall Callback
function shoprex_bootstrap_uninstall_plugin() {
	// Include data file
	require( plugin_dir_path( __FILE__ ) . "shoprex-data.php" );

	foreach ( $jquery_bootstrap_default_settings as $array ) {
		shoprex_bootstrap_delete_db_entry( $array['db_name'] );
	}

	shoprex_bootstrap_delete_db_entry( "shoprex_bootstrap_css_enable" );
	shoprex_bootstrap_delete_db_entry( "shoprex_bootstrap_js_enable" );
	shoprex_bootstrap_delete_db_entry( "shoprex_bootstrap_theme_enable" );


}


// Load Javascript and Styles in Frontend
function shoprex_bootstrap_enqueue_theme_script_and_styles() {

	$shoprex_bootstrap_css_enable   = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_css_enable" ) );
	$shoprex_bootstrap_theme_enable = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_theme_enable" ) );

	if($shoprex_bootstrap_css_enable) {
		$shoprex_bootstrap_css_url = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_css_url" ) );
		if(!empty($shoprex_bootstrap_css_url)) {
			wp_register_style( 'shoprex-bootstrap-css', $shoprex_bootstrap_css_url );
			wp_enqueue_style( 'shoprex-bootstrap-css' );
		}
	}

	if($shoprex_bootstrap_theme_enable) {
		$shoprex_bootstrap_theme_url = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_theme_url" ) );
		if(!empty($shoprex_bootstrap_theme_url)) {
			wp_register_style( 'shoprex-bootstrap-theme', $shoprex_bootstrap_theme_url );
			wp_enqueue_style( 'shoprex-bootstrap-theme' );
		}
	}

	$shoprex_bootstrap_js_enable    = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_js_enable" ) );

	if($shoprex_bootstrap_js_enable) {
		$shoprex_bootstrap_js_url = get_option( shoprex_bootstrap_format_db_keyname( "shoprex_bootstrap_js_url" ) );
		if(!empty($shoprex_bootstrap_js_url)) {
			wp_register_script( 'shoprex-bootstrap-js', $shoprex_bootstrap_js_url, array(), null, true );
			wp_enqueue_script( 'shoprex-bootstrap-js' );
		}
	}
}
add_action( 'wp_enqueue_scripts', 'shoprex_bootstrap_enqueue_theme_script_and_styles' );
