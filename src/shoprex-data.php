<?php
// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


$jquery_bootstrap_default_settings = array(
	"css" => array(
		"cdn_uri" => "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
		"db_name" => "shoprex_bootstrap_css_url"
	),
	"js" => array(
		"cdn_uri" => "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js",
		"db_name" => "shoprex_bootstrap_js_url"
	),
	"theme" => array(
		"cdn_uri" => "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css",
		"db_name" => "shoprex_bootstrap_theme_url"
	),
);
